const config = {
    root: null,
    rootMargin: "20px",
    threshold: 0
};
let observer = new IntersectionObserver(function (entries, self) {
  entries.forEach(entry => {
      if (entry.isIntersecting) 
      {
        dynamicCSS(entry.target);
        dynamicJS(entry.target);
        observer.unobserve(entry.target);
      }
  });
}, config);

const assetsAssets = document.querySelectorAll('.lazy-assets');
assetsAssets.forEach( asset => { observer.observe(asset); });

function dynamicCSS( element )
{
  var link  = document.createElement('link');
  link.rel  = 'stylesheet';
  link.type = 'text/css';
  link.href = element.getAttribute("data-href-css")
  link.media = 'all';
    
  document.head.appendChild(link);
}

function dynamicJS( element )
{
  var script  = document.createElement('script');
  script.src = element.getAttribute("data-href-js")
    
  document.head.appendChild(script);
}


